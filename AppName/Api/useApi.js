const useApi = () => {
  const key = 'd694edc15160401eb9385780a7ecb228';

  const fetchDataforHome = async () => {
    const response = await fetch(
      `https://newsapi.org/v2/top-headlines?country=us&apiKey=${key}`,
    );
    return await response.json();
  };

  const fetchDataForNews = async () => {
    const response = await fetch(
      `https://newsapi.org/v2/everything?q=myanmar&sortBy=publishedAt&apiKey=${key}`,
    );

    return await response.json();
  };

  const fetchSearchData = async value => {
    const response = await fetch(
      `https://newsapi.org/v2/everything?q=${value}&apiKey=d694edc15160401eb9385780a7ecb228`,
    );
    return await response.json();
  };

  return {fetchDataforHome, fetchDataForNews, fetchSearchData};
};

export default useApi;
