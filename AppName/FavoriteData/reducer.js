import {createSlice} from '@reduxjs/toolkit';

const favorite = 'favorite';

export const createFavorite = createSlice({
  name: favorite,
  initialState: {
    favorite: [],
  },

  reducers: {
    addFavorite: (state, {payload}) => {
      const checkAddedFavorite = state.favorite.find(
        fav => fav.title === payload.item.title,
      );

      if (checkAddedFavorite) {
        const addedFavIndex = state.favorite.findIndex(
          fav => fav.title === payload.item.title,
        );
        state.favorite.splice(addedFavIndex, 1);
      } else {
        state.favorite.push(payload.item);
      }
    },
  },
});

export const {addFavorite} = createFavorite.actions;
export default createFavorite.reducer;
