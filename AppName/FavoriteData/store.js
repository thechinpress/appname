import {configureStore} from '@reduxjs/toolkit';
import favoriteReducer from './reducer';

export default configureStore({
  reducer: {
    favorites: favoriteReducer,
  },
});
