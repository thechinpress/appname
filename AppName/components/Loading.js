import React from 'react';
import {View, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';

function Loading({visible = true}) {
  if (!visible) return null;
  return (
    <View style={styles.container}>
      <LottieView autoPlay loop source={require('../assets/loading.json')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Loading;
