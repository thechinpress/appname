import React from 'react';
import {TextInput, View, StyleSheet} from 'react-native';

import AppButton from './AppButton';

function Search({onChangeText, handleSearch, closeSearch}) {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        placeholder="Search"
        placeholderTextColor="gray"
        onChangeText={onChangeText}
      />
      <View style={styles.searchBtnContainer}>
        <AppButton
          title="Search"
          width={120}
          background="gray"
          onPress={handleSearch}
        />
        <AppButton
          title="Close"
          width={120}
          background="red"
          onPress={closeSearch}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  searchInput: {
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    color: '#1a1a1a',
    margin: 10,
  },

  searchBtnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default Search;
