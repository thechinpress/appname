import React from 'react';
import {
  Text,
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
import moment from 'moment';

function ItemDetail({title, content, image, url, date, author}) {
  const handleReadMore = async () => {
    await Linking.openURL(url);
  };

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{uri: image}} />
      <View style={styles.textContainer}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.author}>Author: {author}</Text>
        <Text style={styles.date}>{moment(date).fromNow()}</Text>
        <Text style={styles.content}>{content}</Text>
      </View>
      <TouchableOpacity style={styles.readMore} onPress={handleReadMore}>
        <Text style={styles.read}>Read More</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    height: 260,
    width: '100%',
  },

  textContainer: {
    margin: 10,
  },

  title: {
    fontSize: 19,
    fontWeight: '700',
    paddingBottom: 5,
  },

  author: {
    fontWeight: '700',
  },

  date: {
    color: 'gray',
    fontStyle: 'italic',
    paddingBottom: 10,
  },

  content: {
    fontSize: 16,
  },

  readMore: {
    alignItems: 'center',
    alignSelf: 'center',
  },

  read: {
    color: 'darkblue',
  },
});

export default ItemDetail;
