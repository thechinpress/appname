import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';

function AppButton({onPress, title, width = '100%', background = 'gray'}) {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onPress}
      style={[styles.container, {width: width, backgroundColor: background}]}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderRadius: 25,
    backgroundColor: 'green',
  },

  title: {
    color: '#fff',
    padding: 15,
  },
});

export default AppButton;
