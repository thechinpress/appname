import React from 'react';
import {TextInput, View, StyleSheet} from 'react-native';

function FormInput({onChangeText, value, ...otherProps}) {
  return (
    <TextInput
      style={styles.input}
      {...otherProps}
      onChangeText={onChangeText}
      value={value}
    />
  );
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#fff',
    borderRadius: 15,
    color: '#1a1a1a',
    paddingHorizontal: 10,
    width: '100%',
    marginBottom: 25,
  },
});

export default FormInput;
