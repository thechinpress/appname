import React from 'react';
import {Image, Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import moment from 'moment';

function ItemCard({
  image,
  date,
  description,
  author,
  title,
  onPress,
  height = 300,
}) {
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={[styles.newsContainer, {height: height}]}
      onPress={onPress}>
      <View style={styles.detailContainer}>
        <Text numberOfLines={2} style={styles.title}>
          {title}
        </Text>

        <View style={styles.dateAndAuthorContainer}>
          <Text style={styles.author}>Author : {author}</Text>
          <Text style={styles.date}>{moment(date).fromNow()}</Text>
        </View>
      </View>
      {image ? (
        <Image style={styles.image} source={{uri: image}} />
      ) : (
        <Text numberOfLines={3} style={styles.des}>
          {description}
        </Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  newsContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 1,
    margin: 5,
    overflow: 'hidden',
  },

  image: {
    height: '65%',
    width: '100%',
  },

  detailContainer: {
    padding: 10,
  },

  title: {
    fontWeight: '700',
    fontSize: 18,
    paddingBottom: 5,
  },

  author: {
    fontWeight: '700',
  },

  date: {
    color: 'gray',
    fontStyle: 'italic',
  },
  des: {
    padding: 10,
    fontSize: 17,
  },
});

export default ItemCard;
