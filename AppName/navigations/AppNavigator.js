import React from 'react';
import {TouchableOpacity} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import HomeScreen from '../Screens/HomeScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import NewsScreen from '../Screens/NewsScreen';
import FavoriteScreen from '../Screens/FavoriteScreen';
import DetailScreen from '../Screens/DetailScreen';
import Search from '../Screens/Search';
import LanguageScreen from '../Screens/LanguageScreen';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const Home = ({navigation}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          headerLeft: ({color}) => (
            <TouchableOpacity
              style={{marginLeft: 15}}
              onPress={() => navigation.toggleDrawer()}>
              <Icon name="bars" size={30} color={color} />
            </TouchableOpacity>
          ),

          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        // options={({route}) => ({title: route.params.item.title})}
      />

      <Stack.Screen name="Search" component={Search} />
    </Stack.Navigator>
  );
};

const Favorite = ({navigation}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Favorite"
        component={FavoriteScreen}
        options={{
          headerLeft: ({color}) => (
            <TouchableOpacity
              style={{marginLeft: 15}}
              onPress={() => navigation.toggleDrawer()}>
              <Icon name="bars" size={30} color={color} />
            </TouchableOpacity>
          ),
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        // options={({route}) => ({title: route.params.item.title})}
      />
    </Stack.Navigator>
  );
};

const Profile = ({navigation}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerLeft: ({color}) => (
            <TouchableOpacity
              style={{marginLeft: 15}}
              onPress={() => navigation.toggleDrawer()}>
              <Icon name="bars" size={30} color={color} />
            </TouchableOpacity>
          ),
          headerTitleAlign: 'center',
        }}
      />
    </Stack.Navigator>
  );
};

const News = ({navigation}) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="News"
        component={NewsScreen}
        options={{
          headerLeft: ({color}) => (
            <TouchableOpacity
              style={{marginLeft: 15}}
              onPress={() => navigation.toggleDrawer()}>
              <Icon name="bars" size={30} color={color} />
            </TouchableOpacity>
          ),
          headerTitleAlign: 'center',
        }}
      />
      <Stack.Screen
        name="Detail"
        component={DetailScreen}
        // options={({route}) => ({title: route.params.item.title})}
      />
    </Stack.Navigator>
  );
};

const Language = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Language" component={LanguageScreen} />
    </Stack.Navigator>
  );
};

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="News" component={News} />
        <Drawer.Screen name="Favorite" component={Favorite} />
        <Drawer.Screen name="Language" component={Language} />
        <Drawer.Screen name="Profile" component={Profile} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
