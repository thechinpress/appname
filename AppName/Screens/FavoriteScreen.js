import React from 'react';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';

import ItemCard from '../components/ItemCard';
import Centered from '../components/Centered';

function FavoriteScreen({navigation}) {
  const favoriteItem = useSelector(state => state.favorites.favorite);

  return (
    <>
      {favoriteItem.length === 0 ? (
        <Centered text="No favorite news added!" />
      ) : (
        <FlatList
          data={favoriteItem}
          keyExtractor={item => item.title}
          renderItem={({item}) => (
            <ItemCard
              title={item.title}
              image={item.urlToImage}
              author={item.author}
              date={item.publishedAt}
              onPress={() => navigation.navigate('Detail', {item})}
            />
          )}
        />
      )}
    </>
  );
}

export default FavoriteScreen;
