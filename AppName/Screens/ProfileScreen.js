import React, {useContext} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import AuthContext from '../components/Context';

function ProfileScreen(props) {
  const {user, setUser} = useContext(AuthContext);

  const handleLogout = () => {
    AsyncStorage.removeItem('user');
    setUser(null);
  };

  return (
    <View style={styles.container}>
      <View style={styles.accountContainer}>
        <Icon name="account" size={100} color="lightgray" />
      </View>
      {user && <Text style={styles.name}>{user.name}</Text>}

      {user && (
        <TouchableOpacity style={styles.logout} onPress={handleLogout}>
          <Text>Logout</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    marginTop: 30,
  },

  accountContainer: {
    alignItems: 'center',
    borderRadius: 50,
    borderColor: 'lightgray',
    borderWidth: 1,
    height: 100,
    justifyContent: 'center',
    width: 100,
  },

  name: {
    fontSize: 18,
    fontWeight: '700',
  },

  logout: {
    backgroundColor: '#d9d9d9',
    borderRadius: 10,
    marginTop: 20,
    padding: 10,
  },
});

export default ProfileScreen;
