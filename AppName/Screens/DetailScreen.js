import React, {useEffect} from 'react';
import {
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ItemDetail from '../components/ItemDetail';
import {addFavorite} from '../FavoriteData/reducer';

function DetailScreen({route, navigation}) {
  const {item} = route.params;

  const favoriteItem = useSelector(state => state.favorites.favorite);

  const addedFavorite = favoriteItem.find(
    favItem => favItem.title === item.title,
  );

  const dispatch = useDispatch();

  const handleFavorite = item => {
    // const result = await AsyncStorage.getItem('item');
    // if (result != null) {
    //   await AsyncStorage.mergeItem('item', JSON.stringify(item));
    // }

    // if (result === null) {
    //   await AsyncStorage.setItem('item', JSON.stringify(item));
    // }

    dispatch(addFavorite({item}));
  };

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={styles.addToFavorite}
          onPress={handleFavorite.bind(this, item)}>
          <Icon
            name={addedFavorite ? 'star' : 'star-outline'}
            size={30}
            color="gray"
          />
        </TouchableOpacity>
      ),
    });
  }, [handleFavorite]);

  return (
    <ScrollView style={styles.container}>
      <View>
        <ItemDetail
          title={item.title}
          image={item.urlToImage}
          author={item.author}
          date={item.publishedAt}
          content={item.content}
          url={item.url}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  addToFavorite: {
    marginRight: 20,
  },
});

export default DetailScreen;
