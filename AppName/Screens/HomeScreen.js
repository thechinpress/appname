import React, {useEffect, useState} from 'react';
import {FlatList, Modal, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import useApi from '../Api/useApi';

import ItemCard from '../components/ItemCard';
import Loading from '../components/Loading';
import Search from '../components/Search';

function HomeScreen({navigation}) {
  const [news, setNews] = useState([]);
  const [visible, setVisible] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [loading, setLoading] = useState(false);

  const {fetchDataforHome} = useApi();

  const handleSearch = () => {
    navigation.navigate('Search', {searchText});
    setVisible(false);
  };

  useEffect(() => {
    setLoading(true);
    fetchDataforHome().then(data => {
      setNews(data.articles);
      setLoading(false);
    });
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          style={{marginRight: 15}}
          onPress={() => setVisible(true)}>
          <Icon name="search" size={25} color="gray" />
        </TouchableOpacity>
      ),
    });
  }, [navigation, setVisible]);

  return (
    <>
      <Loading visible={loading} />
      <View>
        <FlatList
          data={news}
          keyExtractor={item => item.title}
          renderItem={({item}) => (
            <ItemCard
              title={item.title}
              date={item.publishedAt}
              image={item.urlToImage}
              author={item.author}
              onPress={() => navigation.navigate('Detail', {item})}
            />
          )}
        />
        <Modal visible={visible} animationType="fade">
          <Search
            handleSearch={handleSearch}
            onChangeText={text => setSearchText(text)}
            closeSearch={() => setVisible(false)}
          />
        </Modal>
      </View>
    </>
  );
}

export default HomeScreen;
