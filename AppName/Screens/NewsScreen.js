import React, {useEffect, useState} from 'react';
import {FlatList, View} from 'react-native';
import useApi from '../Api/useApi';

import ItemCard from '../components/ItemCard';
import Loading from '../components/Loading';

function NewsScreen({navigation}) {
  const [news, setNews] = useState([]);
  const [loading, setLoading] = useState(false);

  const {fetchDataForNews} = useApi();

  useEffect(() => {
    setLoading(true);
    fetchDataForNews().then(data => {
      setNews(data.articles);
      setLoading(false);
    });
  }, []);

  return (
    <>
      <Loading visible={loading} />
      <View>
        <FlatList
          data={news}
          keyExtractor={item => item.title}
          renderItem={({item}) => (
            <ItemCard
              author={item.author}
              title={item.title}
              date={item.publishedAt}
              description={item.description}
              height={200}
              onPress={() => navigation.navigate('Detail', {item})}
            />
          )}
        />
      </View>
    </>
  );
}
export default NewsScreen;
