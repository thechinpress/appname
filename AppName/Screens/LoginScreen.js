import React, {useEffect, useContext} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import auth from '@react-native-firebase/auth';
import {AccessToken, LoginManager} from 'react-native-fbsdk-next';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthContext from '../components/Context';

function LoginScreen(props) {
  const {setUser} = useContext(AuthContext);

  const loginWithFacebook = async () => {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions([
      'public_profile',
      'email',
    ]);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(
      data.accessToken,
    );

    // Sign-in the user with the credential
    const res = await auth().signInWithCredential(facebookCredential);

    const userInfo = res.additionalUserInfo.profile;

    setUser(userInfo);
  };

  return (
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        <Icon name="account" size={90} color="lightgray" />
      </View>
      <TouchableOpacity style={styles.loginBtn} onPress={loginWithFacebook}>
        <Icon name="facebook" size={30} color="#3B5998" />
        <Text style={styles.text}>Login With Facebook</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    flex: 1,
    justifyContent: 'center',
  },

  iconContainer: {
    alignItems: 'center',
    borderColor: 'lightgray',
    borderWidth: 1,
    borderRadius: 50,
    height: 100,
    justifyContent: 'center',
    marginBottom: 20,
    width: 100,
  },

  loginBtn: {
    flexDirection: 'row',
    backgroundColor: '#d9d9d9',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
  },

  text: {
    color: '#000',
    paddingLeft: 10,
  },
});

export default LoginScreen;
