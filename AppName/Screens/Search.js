import React, {useEffect, useState} from 'react';
import {FlatList} from 'react-native';
import useApi from '../Api/useApi';

import ItemCard from '../components/ItemCard';
import Loading from '../components/Loading';
import Centered from '../components/Centered';

function Search({route, navigation}) {
  const {searchText} = route.params;
  const [loading, setLoading] = useState(false);

  const [search, setSearch] = useState([]);
  const {fetchSearchData} = useApi();

  useEffect(() => {
    setLoading(true);
    fetchSearchData(searchText).then(data => {
      setSearch(data.articles);
      setLoading(false);
    });
  }, [searchText]);

  if (loading) {
    return <Loading visible={loading} />;
  }

  return (
    <>
      {search.length === 0 ? (
        <Centered text="No result found!" />
      ) : (
        <FlatList
          data={search}
          keyExtractor={item => item.title}
          renderItem={({item}) => (
            <ItemCard
              title={item.title}
              date={item.publishedAt}
              image={item.urlToImage}
              author={item.author}
              onPress={() => navigation.navigate('Detail', {item})}
            />
          )}
        />
      )}
    </>
  );
}

export default Search;
