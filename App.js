import React, {useEffect, useState} from 'react';
import AppNavigator from './AppName/navigations/AppNavigator';
import {Provider} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import store from './AppName/FavoriteData/store';
import LoginScreen from './AppName/Screens/LoginScreen';
import AuthContext from './AppName/components/Context';
import Loading from './AppName/components/Loading';

function App(props) {
  const [user, setUser] = useState();
  const [loading, setLoading] = useState(false);

  const storeUser = async user => {
    try {
      await AsyncStorage.setItem('user', JSON.stringify(user));
    } catch (error) {
      console.log(error);
    }
  };

  if (user) {
    storeUser(user);
  }

  const getUser = async () => {
    try {
      setLoading(true);

      const result = await AsyncStorage.getItem('user');
      const value = JSON.parse(result);
      if (value != null) {
        setUser(value);
      }

      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getUser();
  }, [setUser]);

  if (loading) {
    return <Loading visible={loading} />;
  }

  return (
    <Provider store={store}>
      <AuthContext.Provider value={{user, setUser}}>
        {user ? <AppNavigator /> : <LoginScreen />}
      </AuthContext.Provider>
    </Provider>
  );
}

export default App;
