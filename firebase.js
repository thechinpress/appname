import {Alert} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
async function requestUserPermission() {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;
  if (enabled) {
    console.log('Authorization status:', authStatus);
  }
}
export const initializeFirebase = async () => {
  messaging()
    .onMessage()
    .messaging()
    .getToken()
    .then(token => {
      console.log('Firebase token: ', token);
    });
  messaging().onTokenRefresh(token => {
    console.log('Firebase token: ', token);
  });
  messaging().setBackgroundMessageHandler(message => {
    Alert.alert('Message!', message);
  });

  // Check whether an initial notification is available

  // Subscribed to topic
  messaging()
    .subscribeToTopic('weather')
    .then(() => console.log('Subscribed to topic!'));
};
